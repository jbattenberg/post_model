import yaml
import keyring

with open("config.yml", "r") as f:
    config = yaml.safe_load(f)

# keyring.set_password('azure_sql_password', 'default', <password>)
AZURE_PASSWORD = keyring.get_password('azure_sql_password', 'default')

AZURE_USER = config["azure"]["user"]
AZURE_DATABASE = config["azure"]["database"]
AZURE_SERVER = config["azure"]["server"]


ACCESS_KEY = config["api"]["access_key"]
SECRET_KEY = config["api"]["secret_key"]
HOST = config["api"]["host"]
AUTHORIZATION = config["api"]["authorization"]
PIPELINE_ID = config["pipeline"]["id"]



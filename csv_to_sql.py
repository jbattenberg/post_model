import pandas as pd
import urllib
import sqlalchemy as sa
import config as cu


# create engine
quoted = urllib.parse.quote_plus(f'Driver={{ODBC Driver 17 for SQL Server}};Server={cu.AZURE_SERVER};Database={cu.AZURE_DATABASE};Uid={cu.AZURE_USER};Pwd={cu.AZURE_PASSWORD};Encrypt=yes;TrustServerCertificate=no')
engine = sa.create_engine(
    f'mssql+pyodbc:///?odbc_connect={quoted}',
    fast_executemany=True)

path_root = r'\\owg.ds.corp\data\Atlanta\Data3\ATLANTA\LifeAct\Shared\03IC Topics\7. Actuarial Transformation\01 OW IC\3. Models, tools, and deliverables\00 Actuarial transformation\Post-model data pipeline\UL Data'


# push to sql
name = "STAT_DETAIL_202103.csv"
dtype={
    "Company Code":str,
    "Major Line":str
    }
df = pd.read_csv(
    path_root+"\\"+name,
    dtype=dtype)

df.to_sql(
    name='STAT_DETAIL_202103',
    con=engine,
    schema = 'raw',
    if_exists='replace',
    index=False)

#
name = "CEDED_RETURN_202103.csv"
dtype={
    "Company Code":str,
    "Major Line":str,
    "Policy ID":str
    }

df = pd.read_csv(
    path_root+"\\"+name,
    dtype=dtype)

df.to_sql(
    name='CEDED_RETURN_202103',
    con=engine,
    schema = 'raw',
    if_exists='replace',
    index=False)

name = "RUN_INFORMATION.csv"
df = pd.read_csv(path_root+"\\"+name)
df=df.iloc[:3]
df.to_sql(
    name='RUN_INFORMATION',
    con=engine,
    schema = 'raw',
    if_exists='replace',
    index=False)

name = "RUN_INFORMATION EXTRA 2.csv"
df = pd.read_csv(path_root+"\\"+name)
df.to_sql(
    name='RUN_INFORMATION_EXTRA_2',
    con=engine,
    schema = 'raw',
    if_exists='replace',
    index=False)

name = "RUN_INFORMATION EXTRA.csv"
df = pd.read_csv(path_root+"\\"+name)
df.to_sql(
    name='RUN_INFORMATION_EXTRA',
    con=engine,
    schema = 'raw',
    if_exists='replace',
    index=False)
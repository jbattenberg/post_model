
{% macro not_null_test(relation=none, col=none) -%}

select
    '{{invocation_id}}' as process_run_id,
    'null' as error_type,
    '{{relation.name}}' as error_table,
    '{{col}}' as error_field,
    'null' as error_value
from {{relation}}
where {{col}} is null   

{% endmacro %}


{% macro relationship_test(from_relation=none, from_col=none, to_relation=none, to_col=none) -%}

select
    '{{invocation_id}}' as process_run_id,
    'relationships' as error_type,
    '{{from_relation.name}}' as error_table,
    '{{from_col}}' as error_field,
    cast(child.{{from_col}} as varchar(50)) as error_value
from {{from_relation}} as child
left join {{to_relation}} as parent
    on child.{{from_col}} = parent.{{to_col}}
where parent.{{to_col}} is null 

{% endmacro %}

{% macro unique_test(relation=none, col=none) -%}

select
    '{{invocation_id}}' as process_run_id,
    'uniqueness' as error_type,
    '{{relation.name}}' as error_table,
    '{{col}}' as error_field,
    cast({{col}} as varchar(50)) as error_value

from {{relation}}
where {{col}} is not null
group by {{col}}
having count(*) > 1

{% endmacro %}

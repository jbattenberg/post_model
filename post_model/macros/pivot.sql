{#
Pivot values from rows to columns.

Example:

    Input: `public.test`

    | size | color |
    |------+-------|
    | S    | red   |
    | S    | blue  |
    | S    | red   |
    | M    | red   |

    select
      size,
      {{ dbt_utils.pivot('color', dbt_utils.get_column_values('public.test',
                                                              'color')) }}
    from public.test
    group by size

    Output:

    | size | red | blue |
    |------+-----+------|
    | S    | 2   | 1    |
    | M    | 1   | 0    |

Arguments:
    column: Column name, required
    values: List of row values to turn into columns, required
    alias: Whether to create column aliases, default is True
    cmp: SQL value comparison, default is =
    prefix: Column alias prefix, default is blank
    suffix: Column alias postfix, default is blank
    then_value: Value to use if comparison succeeds, default is 1
    else_value: Value to use if comparison fails, default is 0
    quote_identifiers: Whether to surround column aliases with double quotes, default is true
    distinct: Whether to use distinct in the aggregation, default is False
#}

{% macro pivotcleanup(relation=none,
               column=none,
               values=none,
               alias=True,
               cmp='=',
               prefix='',
               suffix='',
               then_value=1,
               else_value=0,
               quote_identifiers=True,
               distinct=False) %}
    {{ return(adapter.dispatch('pivotcleanup')(relation, column, values, alias, cmp, prefix, suffix, then_value, else_value, quote_identifiers, distinct)) }}
{% endmacro %}

{% macro default__pivotcleanup(relation, 
               column,
               values,
               alias=True,
               cmp='=',
               prefix='',
               suffix='',
               then_value=1,
               else_value=0,
               quote_identifiers=True,
               distinct=False) %}
{% for fld in values %}
  ,sum("{{ fld }}") as "{{ fld }}"
{% endfor %}
from {{ relation }} as SourceTable

Pivot
(
  sum(SourceTable.{{ then_value }})
  for {{ column }} in (
    {% for fld in values %}
      "{{ fld }}"
       {%- if not loop.last -%}
         ,
       {% endif -%}
    {% endfor %}
  )
)as pivotTable
{% endmacro %}

{% macro athena__pivotcleanup(relation,
               column,
               values,
               alias=True,
               cmp='=',
               prefix='',
               suffix='',
               then_value=1,
               else_value=0,
               quote_identifiers=True,
               distinct=False) %}
  {% for v in values %}
    ,sum(
      {% if distinct %} distinct {% endif %}
      case
      when {{ column }} {{ cmp }} '{{ v }}'
        then {{ then_value }}
      else {{ else_value }}
      end
    )
    {% if alias %}
      {% if quote_identifiers %}
            as "{{ prefix ~ v|replace(" ","_")|replace(" ","_")|replace("/","_")|replace("%","prc")|replace("+","plus")|replace("&","_and_")|replace("(","")|replace(")","")|replace(",","_")|lower ~ suffix }}"
      {% else %}
        as {{prefix ~ v|replace(" ","_")|replace(" ","_")|replace("/","_")|replace("%","_")|replace("+","plus")|replace("&","_and_")|replace("(","")|replace(")","")|replace(",","_")|lower ~ suffix }}
      {% endif %}
    {% endif %}
  {% endfor %}
  from {{ relation }} as sourcetable
{% endmacro %}

{% macro colname_cleanup(column_name, model="") -%}
  {%- if model == ""  -%}
    {{column_name|replace(" ","_")|replace(" ","_")|replace("/","")|replace("%","pct")|replace("+","plus")|replace("&","and")|replace("(","")|replace(")","")|replace(",","_")|replace("*","_")|replace("~","_")|lower}}
  {%- else -%}
    {{column_name|replace(" ","_")|replace(" ","_")|replace("/","")|replace("%","pct")|replace("+","plus")|replace("&","and")|replace("(","")|replace(")","")|replace(",","_")|replace("*","_")|replace("~","_")|lower|replace(model|lower ~ "_","")|replace(model|lower,"")}}
  {%- endif -%}
{%- endmacro %}

{% macro lengthFunction(colName) -%}
  {{ return(adapter.dispatch('lengthFunction')(colName)) }}
{%- endmacro %}

{#
Using default for sql server the - in dbt-sqlserver causes errors in dbt-sqlserver__lengthfunction
#}
{% macro default__lengthFunction(colName) -%}
    len("{{colName}}")
{%- endmacro %}

{% macro athena__lengthFunction(colName) %}
    length("{{colName}}")
{% endmacro %}


{% macro valMonthFilter(colName) -%}
  {{ return(adapter.dispatch('valMonthFilter')(colName)) }}
{%- endmacro %}

{% macro default__valMonthFilter(colName) -%}
    "{{colName}}" like '_[0-9][0-9][A-Z][a-z][a-z]%'
{%- endmacro %}

{% macro athena__valMonthFilter(colName) %}
    regexp_like("{{colName}}",'.[0-9][0-9][A-Z][a-z][a-z]')
{% endmacro %}

{% macro md5Function(colName) -%}
  {{ return(adapter.dispatch('md5Function')(colName)) }}
{%- endmacro %}

{#
Using default for sql server the - in dbt-sqlserver causes errors in dbt-sqlserver__lengthfunction
#}
{% macro default__md5Function(colName) -%}
    HashBytes('MD5', {{colName}})
{%- endmacro %}

{% macro athena__md5Function(colName) %}
    md5(to_utf8({{colName}})
{% endmacro %}


{% macro realNumType() -%}
  {{ return(adapter.dispatch('realNumType')()) }}
{%- endmacro %}

{#
Using default for sql server the - in dbt-sqlserver causes errors in dbt-sqlserver__lengthfunction
#}
{% macro default__realNumType() -%}
    float
{%- endmacro %}

{% macro athena__realNumType() %}
    double
{% endmacro %}
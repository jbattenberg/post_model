{{ config(alias='control'+var('optional_suffix'), materialized='table', tags=['stat_tax']) }}

select 

'{{invocation_id}}' as process_run_id,
'AXIS' as `source`,
count(*) as `count_rows`,
count(policy_num) as `count_policy_num`,
sum(fund_value) as `sum_account_value`,
sum(cash_value) as `sum_cash_value`,
sum(face_amount) as `sum_face_amount`,
sum(net_statutory_reserve) as `sum_reserve`,
'net statutory reserve' as `reserve_description`

from {{ ref('stg_cfile')}} 

Union all

select

'{{invocation_id}}' as process_run_id,
'dbt' as `source`,
count(*) as `count_rows`,
count (distinct rd.policy_num) as `count_policy_num`,
sum(case when rd.reserve_type in ('g_ifv', 'g_gfv') then rd.reserve_amount else 0 end) as `sum_account_value`,
sum(case when rd.reserve_type in ('g_icv', 'g_gcv') then rd.reserve_amount else 0 end) as `sum_cash_value`,
sum(case when rd.reserve_type in ('g_ifa', 'g_gfa') then rd.reserve_amount else 0 end) as `sum_face_amount`,
sum(
    case when rd.reserve_type in (
        's_gbas',
        's_gdef',
        's_gex5a',
        's_gex5d',
        's_gex5e',
        --'s_gex5f',
        's_ibas',
        's_idef',
        's_iex5a',
        's_iex5d',
        's_iex5e'
        --'s_iex5f'
        )
    and rd.reinsurance_type = 'd'
    then rd.reserve_amount else 0 end) +
sum(
    case when rd.reserve_type in (
        's_gbas',
        -- 's_gdef',
        -- 's_gex5a',
        -- 's_gex5d',
        -- 's_gex5e',
        -- 's_gex5f',
        's_ibas'
        -- ,'s_idef',
        -- 's_iex5a',
        -- 's_iex5d',
        -- 's_iex5e',
        -- 's_iex5f'
        -- only basic ceded reserves appear to come into this calculation
        )
    and rd.reinsurance_type = 'c'
    then rd.reserve_amount else 0 end)
as `sum_reserve`,
'net statutory reserve' as `reserve_description`
from {{ ref('reserve_detail')}} as rd
inner join {{ ref('ul_inforce')}} as inf
on rd.policy_num = inf.policy_num


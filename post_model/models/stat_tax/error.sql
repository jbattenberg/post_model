{{ config(alias='error'+var('optional_suffix'), materialized='table', tags=['stat_tax']) }}

-- not null test
{{ not_null_test(ref('reserve_type'), 'reserve_type') }}
union all
{{ not_null_test(ref('reserve_type'), 'reinsurance_type') }}
union all
{{ not_null_test(ref('ul_group_ind_mapping'), 'line_of_business') }}
union all
{{ not_null_test(ref('ul_group_ind_mapping'), 'valuation_date') }}
union all
{{ not_null_test(ref('ul_legal_entity_mapping'), 'legal_entity_code') }}
union all
{{ not_null_test(ref('ul_legal_entity_mapping'), 'valuation_date') }}
union all
{{ not_null_test(ref('reserve_detail'), 'valuation_date') }}
union all
{{ not_null_test(ref('reserve_detail'), 'policy_num') }}
union all
{{ not_null_test(ref('reserve_detail'), 'reserve_type') }}
union all
{{ not_null_test(ref('reserve_detail'), 'center') }}
union all
{{ not_null_test(ref('reserve_detail'), 'reinsurance_type') }}
union all
{{ not_null_test(ref('reserve_aggregate'), 'valuation_date') }}
union all
{{ not_null_test(ref('reserve_aggregate'), 'reserve_type') }}
union all
{{ not_null_test(ref('reserve_aggregate'), 'reinsurance_type') }}
union all
{{ not_null_test(ref('init_process_run_book'), 'process_run_id') }}
union all
{{ not_null_test(ref('control'), 'process_run_id') }}
union all
{{ not_null_test(ref('ul_inforce'), 'policy_num') }}
union all
{{ not_null_test(ref('ul_inforce'), 'valuation_date') }}
union all
{{ not_null_test(ref('model_run_book'), 'model_run_id') }}

-- relationship test
union all
{{ relationship_test(ref('init_process_run_book'), 'model_run_id', ref('model_run_book'), 'model_run_id' )}}
union all
{{ relationship_test(ref('reserve_detail'), 'policy_num', ref('ul_inforce'), 'policy_num' )}}
union all
{{ relationship_test(ref('reserve_detail'), 'process_run_id', ref('init_process_run_book'), 'process_run_id' )}}
union all
{{ relationship_test(ref('init_process_run_book'), 'model_run_id', ref('model_run_book'), 'model_run_id' )}}

-- unique test
union all
{{ unique_test(ref('init_process_run_book'), 'process_run_id')}}
union all
{{ unique_test(ref('reserve_type'), 'concat(CAST(reserve_type AS varchar(50)),CAST(reinsurance_type as varchar(50)))')}}
union all
{{ unique_test(ref('ul_group_ind_mapping'), 'concat(CAST(line_of_business AS varchar(50)),CAST(valuation_date as varchar(50)))')}}
union all
{{ unique_test(ref('ul_legal_entity_mapping'), 'concat(CAST(legal_entity_code AS varchar(50)),CAST(valuation_date as varchar(50)))')}}
union all
{{ unique_test(ref('reserve_aggregate'), 'concat(CAST(valuation_date AS varchar(50)),CAST(reserve_type AS varchar(50)),CAST(reinsurance_type AS varchar(50)))')}}
union all
{{ unique_test(ref('init_process_run_book'), 'process_run_id')}}
union all
{{ unique_test(ref('ul_inforce'), 'concat(CAST(policy_num AS varchar(50)),CAST(valuation_date AS varchar(50)))')}}
union all
{{ unique_test(ref('model_run_book'), 'model_run_id')}}

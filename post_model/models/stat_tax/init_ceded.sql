{{ config(alias='init_ceded', materialized='view', tags=['stat_tax']) }}


select 
    cc.*,
    rb.`process_run_id`,
    mb.`valuation_date`,
    cf.`line_of_business`,
    concat(left(cf.`center`,5), rm.`rein_center`) as `center`

from {{ ref('stg_ceded')}} as cc
    inner join {{ ref('init_process_run_book')}} as rb
    on cc.`model_run_id` = rb.`model_run_id`
    inner join {{ ref('model_run_book')}} as mb
    on cc.`model_run_id` = mb.`model_run_id`
    inner join {{ ref('init_cfile')}} as cf
    on cc.`policy_num` = cf.`policy_num`
    inner join {{ ref('reinsurance_mapping')}} as rm
    on cc.`center_mapping_code` = rm.`mapping_code`   
{{ config(alias='init_cfile', materialized='view', tags=['stat_tax']) }}


with aggregated_wop as (
    select policy_num,
    sum(exh5f_gross_wop_rider_dis_stat_reserve) as exh5f_gross_wop_rider_dis_stat_reserve,
    valuation_date
    from {{ref('ul_wop_reserve')}} as wop
    group by policy_num,
    valuation_date
)

select 
    cc.*,
    case
        when cc.`fund_value` < 0 then 0
        else cc.`fund_value`
    end as `floored_fund_value`,
    rb.`process_run_id`,
    mb.`valuation_date`,
    case 
        when cc.`issue_year` > '1991' and cc.`issue_year` < '2000' Then 'CA'
        else 'Non-CA'
    end as `ca_method`,
    svl.`val_rate`,
    coalesce(wop.`exh5f_gross_wop_rider_dis_stat_reserve`,0) as `exh5f_gross_wop_rider_dis_stat_reserve`

from {{ ref('stg_cfile')}} as cc
    inner join {{ ref('init_process_run_book')}} as rb
    on cc.`model_run_id` = rb.`model_run_id`
    inner join {{ ref('model_run_book')}} as mb
    on cc.`model_run_id` = mb.`model_run_id`
    left join {{ref('ul_svl_mapping_exh5')}} as svl
    on cc.`issue_year` = svl.`issue_year`
    left join aggregated_wop as wop
    on cc.`policy_num` = wop.`policy_num` and mb.valuation_date = wop.valuation_date


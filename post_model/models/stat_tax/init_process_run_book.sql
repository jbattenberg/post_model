{{ config(alias='init_process_run_book'+var('optional_suffix'), materialized='view', tags=['stat_tax']) }}

with runbook as (
    select distinct cast(`model_run_id` as varchar(16)) as `model_run_id`
    from {{ref('stg_cfile')}}
)

select 
    cast('{{invocation_id}}' as varchar(36)) as `process_run_id`,
    `model_run_id`,
    'NotApproved' as `fin_rep_approval_ind`,
    '' as `fin_rep_approval_by`,
    '{{var('function_id_stat_tax')}}' as `function_id`,
    '{{var('function_name_stat_tax')}}' as `function_name`,
    '{{var('orchestrator_id')}}' as `orchestrator_id`,
    '{{var('orchestrator_name')}}' as `orchestrator_name`,
    current_date() as `process_record_date`,
    cast(current_date() as date) as `start_date`,
    cast('9999-12-31' as date) as `end_date` 
from runbook
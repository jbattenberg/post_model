{{ config(alias='key_control'+var('optional_suffix'), materialized='table', tags=['stat_tax']) }}

select 
    rb.`process_run_id`,
    'N' as `kc2_flag`,
    'N' as `kc4_flag`,
    'N' as `kc6_flag`,
    'N' as `kc9_flag`,
    'N' as `kc10_flag`

from {{ ref('init_process_run_book')}} as rb
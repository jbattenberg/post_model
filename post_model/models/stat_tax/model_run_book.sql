{{ config(alias='model_run_book'+var('optional_suffix'), materialized='table', tags=['stat_tax']) }}

select 
    rb.`model_run_id`,
    '{{var('model_id')}}' as `model_id`,
    ri.`job_id`,
    ri.`job_name`,
    ri.`dataset` as `dataset_name`,
    last_day(concat(ri.`valuationyear`, '-',ri.`valuationmonth`, '-', '01')) as `valuation_date`,
    ri.`timestamp` as `run_timestamp`,
    ri.`username` as `user_name`,
    '' as `run_description`
    
from {{ ref('init_process_run_book')}} as rb
inner join
{{ ref('stg_run_information')}}
as ri
on rb.`model_run_id` = ri.`run_id`
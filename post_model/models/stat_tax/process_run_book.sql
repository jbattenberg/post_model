{{ config(alias='process_run_book'+var('optional_suffix'), materialized='table', tags=['stat_tax']) }}

{% set error_dict = dbt_utils.get_query_results_as_dict("select * from " ~ ref('error')) %}

select
    rb.*,
    {% if error_dict['error_field']|length > 0 %}
    'Y' as process_error_flag,
    '{{error_dict | tojson }}' as process_error_description
    {%else%}
    'N' as process_error_flag,
    '' as process_error_description
    {%endif%}

from {{ref('init_process_run_book')}} as rb
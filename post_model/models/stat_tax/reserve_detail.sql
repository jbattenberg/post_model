{{ config(alias='reserve_detail'+var('optional_suffix'), materialized='table', tags=['stat_tax']) }}

{% set exclude_cfile = ["valuation_date", "policy_num", "process_run_id", "model_run_id", "line_of_business", "center", "val_rate", "admin_plan_code", "ca_method"] %}
{% set exclude_ceded = ["valuation_date", "policy_num", "process_run_id", "model_run_id", "line_of_business", "center", "exh5_code"] %}
{% set include_cfile = get_query_results_as_dict("select distinct report_field from " ~ ref('reserve_mapping')) %}
{% set include_ceded = get_query_results_as_dict("select distinct report_field from " ~ ref('ceded_reserve_mapping')) %}


with unpivot_cfile as (
    {{ unpivotwquotes(ref('init_cfile'), field_name='report_field', value_name='reserve_amount', exclude_cols=exclude_cfile, include_cols=include_cfile.report_field) }}
),

unpivot_ceded as (
    {{ unpivotwquotes(ref('init_ceded'), field_name='report_field', value_name='reserve_amount', exclude_cols=exclude_ceded, include_cols=include_ceded.report_field) }}
),

mapped_cfile as (
    select    
        `valuation_date`,
        `policy_num`,
        rm.`reinsurance_type`,
        rm.`reserve_type`,
        `process_run_id`,
        ut.`reserve_amount` * rm.`scalar` as `reserve_amount`,
        '{{var('product_group')}}' as `product_group`,
        case 
            when rt.`exhibit_section` = 'A' then
                    concat(map.`mortality_table`,
                    ' CSO ALB ', 
                    case
                        when map.`interest_rate` = 'SVL' then ut.`val_rate`
                        else map.`interest_rate`
                    end,
                    ' CRVM ',
                    map.`death_method`)
            else valstd.`valuation_standard`
        end as `valuation_standard`,
        'modeled' as `modeled_ind`,
        ut.`center`
    from unpivot_cfile as ut
    inner join {{ ref('reserve_mapping')}} as rm
        on (ut.`report_field`=rm.`report_field` and ut.`line_of_business` = rm.`line_of_business`)
    inner join {{ref('reserve_type')}} as rt
        on rm.`reserve_type` = rt.`reserve_type` and rm.`reinsurance_type` = rt.`reinsurance_type`
    left join {{ref('ul_valuation_standard_exh5')}} as valstd
        on rt.`exhibit_section` = valstd.`exhibit_section`
    left join {{ref('ul_plancode_mapping_exh5')}} as map
        on (ut.`admin_plan_code` = map.`admin_plan_code` and ut.`ca_method` = map.`CA_Method`)
),

mapped_ceded as (
    select    
        `valuation_date`,
        `policy_num`,
        rm.`reinsurance_type`,
        rm.`reserve_type`,
        `process_run_id`,
        `reserve_amount` * rm.`scalar` as `reserve_amount`,
        '{{var('product_group')}}' as `product_group`,
        'REINSURANCE CEDED' as `valuation_standard`,
        'modeled' as `modeled_ind`,
        `center`
    from unpivot_ceded as ut
    inner join {{ ref('ceded_reserve_mapping')}} as rm
        on ut.`report_field`=rm.`report_field` and ut.`line_of_business` = rm.`line_of_business` and ut.`exh5_code` = rm.`exh5_code`

),

grouped_cfile as (
    select
        `valuation_date`,
        `policy_num`,
        `reserve_type`,
        `center`,
        `reinsurance_type`,
        `process_run_id`,
        sum(`reserve_amount`) as `reserve_amount`,
        `valuation_standard`,
        `product_group`,
        `modeled_ind`
    from mapped_cfile
    group by
        `valuation_date`,
        `policy_num`,
        `reserve_type`,
        `center`,
        `reinsurance_type`,
        `process_run_id`,
        `valuation_standard`,
        `product_group`,
        `modeled_ind`
),

grouped_ceded as (
    select
        `valuation_date`,
        `policy_num`,
        `reserve_type`,
        `center`,
        `reinsurance_type`,
        `process_run_id`,
        sum(`reserve_amount`) as `reserve_amount`,
        `valuation_standard`,
        `product_group`,
        `modeled_ind`
    from mapped_ceded
    group by
        `valuation_date`,
        `policy_num`,
        `reserve_type`,
        `center`,
        `reinsurance_type`,
        `process_run_id`,
        `valuation_standard`,
        `product_group`,
        `modeled_ind`
)

select * from grouped_cfile where reserve_amount <> 0
union all
select * from grouped_ceded where reserve_amount <> 0

{{ config(alias='ceded', materialized='table', tags=['stat_tax']) }}

{% set columns_dict = dbt_utils.get_query_results_as_dict("select column_name, column_rename from " ~ ref('axis_reserve_columns') ~ " where source_table ='ceded'") %}


with cleaned_ceded as (
select
    {% for rownum in range(columns_dict.column_name|count) -%}
        cc.`{{columns_dict.column_name[rownum]}}` as `{{columns_dict.column_rename[rownum]}}`
        {% if not loop.last %},{% endif %}
    {%- endfor %}
 
from {{source('raw_axis_results_reserve', var('ceded'))}} as cc
)

select
    cl.*,
    cf.model_run_id
from cleaned_ceded as cl
inner join {{ref('stg_cfile')}} as cf
on cl.policy_num = cf.policy_num

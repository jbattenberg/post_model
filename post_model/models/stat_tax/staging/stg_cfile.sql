{{ config(alias='cfile', materialized='table', tags=['stat_tax']) }}

{% set columns_dict = dbt_utils.get_query_results_as_dict("select column_name, column_rename from " ~ ref('axis_reserve_columns') ~ " where source_table ='cfile'") %}

with cleaned_cfile as (
    select
        {% for rownum in range(columns_dict.column_name|count) -%}
            trim(cf.`{{columns_dict.column_name[rownum]}}`) as `{{columns_dict.column_rename[rownum]}}`
            {% if not loop.last %},{% endif %}
        {%- endfor %}

    from {{source('raw_axis_results_reserve', var('cfile'))}} as cf
)

select
    cc.*,
    cl.center,
    cl.line_of_business
from cleaned_cfile as cc
inner join {{ref('center_lobb_mapping')}} as cl
on cc.company_code = cl.company and cc.rlob = cl.rlob and cc.major_line = cl.major_line

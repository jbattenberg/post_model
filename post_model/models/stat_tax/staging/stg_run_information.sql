{{ config(materialized='table', tags=['stat_tax']) }}
{%- set ricols = adapter.get_columns_in_relation( source('raw_axis_results_reserve', "RUN_INFORMATION")) %}
{%- set exclcols = ["Notepad", "Version", "ValuationYear", "ValuationMonth"] %}

select
{% if var('run_type') == 'current' %}

    {% for col in ricols -%}
        {% if col.column not in exclcols %}
            `{{col.column}}` as `{{colname_cleanup(col.column)}}`,
        {% endif %}  
    {%- endfor %}
    `Version` as `axis_version`,
    cast(`valuationyear` as int) as `valuationyear`,
    cast(`valuationmonth` as int) as `valuationmonth`

    from
    {{ source('raw_axis_results_reserve', "RUN_INFORMATION")}}

{% else %}
    * from {{ ref('supp_run_information')}}
    
{% endif %}

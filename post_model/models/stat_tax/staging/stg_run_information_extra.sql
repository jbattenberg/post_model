{{ config(materialized='table', tags=['stat_tax']) }}
{%- set ricols = adapter.get_columns_in_relation( source('raw_axis_results_reserve', "RUN_INFORMATION_EXTRA")) %}

select

{% for col in ricols -%}
    `{{col.column}}` as `{{colname_cleanup(col.column)}}`
    {% if not loop.last%} , {% endif %}
{%- endfor %}

from {{ source('raw_axis_results_reserve', "RUN_INFORMATION_EXTRA")}}

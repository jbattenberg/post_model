{{ config(alias='ul_inforce'+var('optional_suffix'), materialized='table', tags=['stat_tax']) }}

{% set inforce_cols = dbt_utils.get_query_results_as_dict("select column_rename from " ~ ref('axis_reserve_columns') ~ " where inforce ='TRUE'") %}
{% set cfcols = adapter.get_columns_in_relation( ref('init_cfile')) %}

{%- set include_cols = [] %}
{%- for col in cfcols -%}
    {%- if col.column in inforce_cols.column_rename -%}
        {% do include_cols.append(col.column) %}
    {%- endif %}
{%- endfor %}

select

{{include_cols|join(',')}}
, `valuation_date`
, `line_of_business`
, '{{ var('legacy_block')}}' as `legacy_block`
from {{ ref('init_cfile')}}


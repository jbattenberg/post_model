import requests
import config as cu
import math

url = "https://us2.hevodata.com/api/public/v2.0/pipelines/2/run-now"


def call_hevo_api(url:str):
    """Call Hevo Data API and return response

    Args:
        url (str): request url

    Raises:
        Exception: connection failed

    Returns: API response object
    """
    headers = {
        "Accept": "application/json",
        "Authorization": cu.AUTHORIZATION
    }

    response = requests.get(url, headers=headers)

    if math.floor(response.status_code/100) != 2: #ensure response is 2xx
        return Exception("API Connection failed")
    else:
        return response


def run_hevo_pipeline(id:int):
    """Use Hevo API to run a pipeline

    Args:
        id (int): Pipeline ID

    Returns:
        _type_: Prints confirmation of pipeline run
    """

    url = cu.HOST + f"/api/public/v2.0/pipelines/{id}/run-now"
    call_hevo_api(url)

    return print(f"Pipeline {id} kicked off")
